var BlogPagesSchema = require('../models/blog-pages');

// list all blogs
module.exports.listAllBlogPages = function(req, res) {
    BlogPagesSchema
    .find().sort({createdAt: 'desc'}).find({}, function(err, listallblogs){
        if(err){
          console.log(err);
        } else {
            res.json(listallblogs);
        }
    });
};
