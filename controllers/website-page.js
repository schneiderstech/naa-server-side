var WebSitePagesSchema = require('../models/website-page');

// list website page content
module.exports.listPage = function(req, res) {
    WebSitePagesSchema
    .find({ url: req.url }, function(err, listpage){
        if(err){
          console.log(err);
        } else {
            res.json(listpage);
        }
    });
};
