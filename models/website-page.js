var mongoose = require('mongoose');


var ImageContent = new mongoose.Schema({
    image: [],
    alt: [],
    path: []
});

var WebSitePagesSchema = new mongoose.Schema({
    title: {
        type: String,
    },
    subtitle: {
        type: String
    },
    url: {
        type: String,
    },    
    keywords: [],
    headline: [],
    content: [],
    images: [ImageContent]
});

mongoose = require('mongoose').set('debug', true);


module.exports = mongoose.model('website-pages', WebSitePagesSchema);
