var mongoose = require('mongoose');


var BlogImage = new mongoose.Schema({
    image: [],
    alt: [],
    path: []
});

var BlogPagesSchema = new mongoose.Schema({
    title: {
        type: String,
    },
    subtitle: {
        type: String
    },
    url: {
        type: String,
    },    
    keywords: [],
    headline: [],
    content: [],
    images: [BlogImage],
    likes: {
        type: Number
    }
}, {
    timestamps: true
});

mongoose = require('mongoose').set('debug', true);


module.exports = mongoose.model('blog-pages', BlogPagesSchema);
