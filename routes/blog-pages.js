var express = require('express');
var router  = express.Router();
var bodyParser = require('body-parser');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

var BlogPagesController = require('./../controllers/blog-pages');

router.get('/blog-pages', BlogPagesController.listAllBlogPages);

module.exports = router;