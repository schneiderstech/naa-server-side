var express = require('express');
var router  = express.Router();
var bodyParser = require('body-parser');

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: false }));

var WebSitePageController = require('./../controllers/website-page');

router.get('/contact', WebSitePageController.listPage);

module.exports = router;