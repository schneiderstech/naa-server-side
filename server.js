var express     = require('express');
var path        = require('path');
var bodyParser  = require('body-parser');
var favicon     = require('serve-favicon');
var databaseConfig = require('./config/database');
var mongoose    = require('mongoose');

var index       = require('./routes/index');
var aboutPage   = require('./routes/about');
var contactPage = require('./routes/contact');
var blogPage    = require('./routes/blog');
var blogPages   = require('./routes/blog-pages');

var port = 8080;
var app = express();


// CONNECTION EVENTS
mongoose.connect(databaseConfig.url);

// When successfully connected
mongoose.connection.on('connected', function () {  
  console.log('Mongoose default connection open to ' + databaseConfig.url);
}); 

// If the connection throws an error
mongoose.connection.on('error',function (err) {  
  console.log('Mongoose default connection error: ' + err);
}); 

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {  
  console.log('Mongoose default connection disconnected'); 
});

// View Engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);

//Set static folder
// add static files from client-side directory
app.use(express.static(path.join(__dirname, '..', '/naa-client-side/src')));
app.use(express.static(path.join(__dirname, '..', '/naa-client-side/node_modules')));

// Body Parser 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(favicon(path.join(__dirname, '..', '/naa-client-side/src/assets/favicon.ico')));

app.use('/', index);
app.use('/', aboutPage);
app.use('/', contactPage);
app.use('/', blogPage);
app.use('/', blogPages);

app.listen(port, function() {
    console.log('Server started on port ' + port);
});
